package pl.edu.pwsztar.service.impl;

import pl.edu.pwsztar.data.DataManager;
import pl.edu.pwsztar.data.impl.DataManagerImpl;
import pl.edu.pwsztar.domain.find.FindProduct;
import pl.edu.pwsztar.domain.find.impl.FindProductImpl;
import pl.edu.pwsztar.domain.model.Cart;
import pl.edu.pwsztar.domain.model.Product;
import pl.edu.pwsztar.service.ShoppingCartOperation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ShoppingCart implements ShoppingCartOperation {
    private final DataManager data;
    private final FindProduct findProduct;

    public ShoppingCart(){
        this.data = new DataManagerImpl();
        this.findProduct = new FindProductImpl();
        data.saveCart(new Cart.Builder().products(new ArrayList<>()).build());
    }

    public boolean addProducts(String productName, int price, int quantity) {
        Cart dataCart = data.getCart();
        List<Product> products = null;
        Product product = new Product.Builder().productName(productName).price(price).quantity(quantity).build();
        Product oldProductData = null;
        int fullQuantity = 0;
        boolean result = false;

        if(price <= 0 || quantity <= 0 || quantity > PRODUCTS_LIMIT){
            return false;
        }

        oldProductData = findProduct.findProduct(dataCart, productName);

        for(Product dataProduct: dataCart.getProducts()){
            fullQuantity += dataProduct.getQuantity();
            if(fullQuantity > PRODUCTS_LIMIT-1){
                return false;
            }
        }

        if (oldProductData != null && (oldProductData.getPrice() != price || oldProductData.getQuantity()+quantity > PRODUCTS_LIMIT)) {
            return false;
        } else if(oldProductData == null){
            Optional<List<Product>> checkProducts = Optional.ofNullable(dataCart).map(Cart::getProducts);
            products = checkProducts.orElseGet(ArrayList::new);
            products.add(product);
            result = true;
        }else{
            products = new ArrayList<>();
            for(Product oldProduct: dataCart.getProducts()){
                if(oldProduct.getProductName().equals(productName)){
                    oldProduct = new Product.Builder(oldProduct).quantity(oldProduct.getQuantity()+quantity).build();
                    result = true;
                }
                products.add(oldProduct);
            }
        }

        if(result) {
            data.saveCart(new Cart.Builder().products(products).build());
        }

        return result;
    }

    public boolean deleteProducts(String productName, int quantity) {
        Cart dataCart = data.getCart();
        List<Product> products = new ArrayList<>();
        Product oldProductData = null;
        boolean result = false;

        if(quantity <= 0 || quantity > PRODUCTS_LIMIT){
            return false;
        }

        oldProductData = findProduct.findProduct(dataCart, productName);

        if (oldProductData == null || oldProductData.getQuantity()-quantity < 0) {
            return false;
        }else{
            for(Product product: dataCart.getProducts()){
                if(product.getProductName().equals(productName)){
                    if(oldProductData.getQuantity()-quantity != 0) {
                        product = new Product.Builder().productName(productName).price(product.getPrice()).quantity(oldProductData.getQuantity() - quantity).build();
                        products.add(product);
                    }
                    result = true;
                }else{
                    products.add(product);
                }
            }
        }

        if(result) {
            data.saveCart(new Cart.Builder().products(products).build());
        }

        return result;
    }

    public int getQuantityOfProduct(String productName) {
        Cart dataCart = data.getCart();
        Product oldProductData = null;

        oldProductData = findProduct.findProduct(dataCart, productName);

        if(oldProductData == null){
            return 0;
        }

        return oldProductData.getQuantity();
    }

    public int getSumProductsPrices() {
        Cart dataCart = data.getCart();
        int summarized = 0;

        if(dataCart.getProducts().isEmpty()){
            return summarized;
        }

        for(Product product: dataCart.getProducts()){
            summarized += product.getPrice() * product.getQuantity();
        }

        return summarized;
    }

    public int getProductPrice(String productName) {
        Cart dataCart = data.getCart();
        Product oldProductData = null;

        oldProductData = findProduct.findProduct(dataCart, productName);

        if(oldProductData == null){
            return 0;
        }

        return oldProductData.getPrice();
    }

    public List<String> getProductsNames() {
        Cart dataCart = data.getCart();
        List<String> productsNames = new ArrayList<>();


        for(Product product: dataCart.getProducts()){
            productsNames.add(product.getProductName());
        }

        return productsNames;
    }
}
