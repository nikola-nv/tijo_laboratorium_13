package pl.edu.pwsztar.data.impl;

import pl.edu.pwsztar.data.FileMain;
import pl.edu.pwsztar.domain.model.Cart;

import java.io.*;
import java.util.ArrayList;

public class FileMainImpl implements FileMain{

    @Override
    public File createDataFile() {
        File file = null;
        ObjectOutputStream ois = null;

        try {
            file = new File("cart_data.txt");

            if (!file.exists()) {
                file.createNewFile();
            }

            if (file.isFile() && file.length() == 0) {
                ois = new ObjectOutputStream(new FileOutputStream(file));
                ois.writeObject(new Cart.Builder().products(new ArrayList<>()).build());
            }
            return file;
        }
        catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(ois!=null) {
                    ois.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
