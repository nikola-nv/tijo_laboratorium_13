package pl.edu.pwsztar.data.impl;

import pl.edu.pwsztar.data.DataManager;
import pl.edu.pwsztar.data.FileMain;
import pl.edu.pwsztar.domain.model.Cart;

import java.io.*;
import java.util.ArrayList;

public class DataManagerImpl implements DataManager {
    private final FileMain fileMain;

    public DataManagerImpl(){
        this.fileMain = new FileMainImpl();
    }

    @Override
    public void saveCart(Cart cart) {
        ObjectOutputStream oos = null;
        File file = fileMain.createDataFile();

        try{
            oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(cart);
        }
        catch (IOException e){
            e.printStackTrace();
        }
        finally {
            try {
                if(oos!=null) {
                    oos.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Cart getCart() {
        ObjectInputStream ois = null;
        File file = fileMain.createDataFile();
        Cart products = null;

        try{
            ois = new ObjectInputStream(new FileInputStream(file));

                try {
                    products = ((Cart) ois.readObject());
                    return products;
                }catch (EOFException e){
                    e.printStackTrace();
                }

        }
        catch(IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        finally {
            try {
                if(ois!=null) {
                    ois.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }

        }
        return null;
    }

    @Override
    public void clearData() {
        ObjectOutputStream oos = null;
        File file = fileMain.createDataFile();

        try{
            oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(new Cart.Builder().products(new ArrayList<>()).build());
        }
        catch (IOException e){
            e.printStackTrace();
        }
        finally {
            try {
                if (oos != null) {
                    oos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
