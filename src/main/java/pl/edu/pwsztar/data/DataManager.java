package pl.edu.pwsztar.data;

import pl.edu.pwsztar.domain.model.Cart;

public interface DataManager {
    void saveCart(Cart accounts);
    Cart getCart();
    void clearData();
}
