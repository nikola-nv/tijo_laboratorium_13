package pl.edu.pwsztar.domain.model;

import java.io.Serializable;
import java.util.List;

public class Cart implements Serializable {
    private List<Product> products;

    private Cart(Builder builder){
        this.products = builder.products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public static final class Builder{
        private List<Product> products;

        public Builder products(List<Product> cart){
            this.products = cart;
            return this;
        }

        public Cart build(){
            return new Cart(this);
        }
    }
}
