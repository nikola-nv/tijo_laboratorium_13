package pl.edu.pwsztar.domain.model;

import java.io.Serializable;

public class Product implements Serializable {
    private String productName;
    private int price;
    private int quantity;

    private Product(Builder builder){
        this.productName = builder.productName;
        this.price = builder.price;
        this.quantity = builder.quantity;
    }

    public String getProductName() {
        return productName;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public static final class Builder{
        private String productName;
        private int price;
        private int quantity;

        public Builder(){

        }

        public Builder(Product product){
            this.productName = product.getProductName();
            this.price = product.getPrice();
            this.quantity = product.getQuantity();
        }

        public Builder productName(String productName){
            this.productName = productName;
            return this;
        }

        public Builder price(int price){
            this.price = price;
            return this;
        }

        public Builder quantity(int quantity){
            this.quantity = quantity;
            return this;
        }

        public Product build(){
            return new Product(this);
        }
    }
}
