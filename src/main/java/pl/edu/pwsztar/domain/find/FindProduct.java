package pl.edu.pwsztar.domain.find;

import pl.edu.pwsztar.domain.model.Cart;
import pl.edu.pwsztar.domain.model.Product;

public interface FindProduct {
    Product findProduct(Cart cart, String productName);
}
