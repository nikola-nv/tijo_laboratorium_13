package pl.edu.pwsztar.domain.find.impl;

import pl.edu.pwsztar.domain.find.FindProduct;
import pl.edu.pwsztar.domain.model.Cart;
import pl.edu.pwsztar.domain.model.Product;

public class FindProductImpl implements FindProduct {
    @Override
    public Product findProduct(Cart cart, String productName) {
        for(Product dataProduct: cart.getProducts()){
            if(dataProduct.getProductName().equals(productName)){
                return new Product.Builder(dataProduct).build();
            }
        }
        return null;
    }
}
